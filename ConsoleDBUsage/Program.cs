﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDBUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            //call this only one time when creating a brand new table.
            //CheckLocalDBConnection();

            //call this to insert
            InsertRow();

            Console.WriteLine("All done");
            Console.ReadLine();
        }

        public static void CheckLocalDBConnection()
        {

            //connection string for online database
            //connectionString="Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-MvcMovie;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\Movies.mdf"
            //connectionSTring = @"Data Source = <server address>; Initial Catalog=<database name>; Persistent Security Info = True; User ID = <user id>; Password = <password her>";

            //connectiong string for local database. NOTE : There is no Initial Catalog. 
            var connectionString = @"Data Source = demoserver1shwe.database.windows.net; Initial Catalog=demodb1;  User ID = adminshwe; Password = Password$123";
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();

            SqlCommand sqlCommand = new SqlCommand("CREATE TABLE Persons (PersonID int,FirstName varchar(255))");
            //            / INSERT INTO Customers(CustomerName, ContactName, Address, City, PostalCode, Country)
            //VALUES('Cardinal', 'Tom B. Erichsen', 'Skagen 21', 'Stavanger', '4006', 'Norway');
            //SqlCommand sqlCommand = new SqlCommand("INSERT INTO Persons(PersonID,FirstName) VALUES(2,'Jay');");
            sqlCommand.Connection = Con;
            var response = sqlCommand.ExecuteNonQuery();


            //// Open the connection
            //conn.Open();

            //// 1. Instantiate a new command with a query and connection
            //SqlCommand cmd = new SqlCommand("select CategoryName from Categories", conn);

            //// 2. Call Execute reader to get query results
            //rdr = cmd.ExecuteReader();

            //// print the CategoryName of each record
            //while (rdr.Read())
            //{
            //    Console.WriteLine(rdr[0]);
            //}

            var conn = Con;
            SqlCommand cmd = new SqlCommand("select FirstName from Persons", conn);

            // 2. Call Execute reader to get query results
            var rdr = cmd.ExecuteReader();
            var numberofrows = 0;
            var listofnames = new List<string>();
            // print the CategoryName of each record
            while (rdr.Read())
            {

                var temp = rdr[0].ToString();
                listofnames.Add(temp);
                numberofrows++;
            }

            var totalrows = numberofrows;
            var finalnames = listofnames;



            Con.Close();

        }

        public static void InsertRow()
        {
            var functionname = "InsertRow";
            var welcomemessage = "Entering "+functionname;
            var leavingmessage = "Leaving " + functionname;
            Console.WriteLine(welcomemessage);
            


            var connectionString = @"Data Source = demoserver1shwe.database.windows.net; Initial Catalog=demodb1;  User ID = adminshwe; Password = Password$123";
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();

            var nameOfTable = "Persons";

            var idOfPerson = 2;
            var nameOFPerson = "Bruce Wayne";

            var commandString = @"INSERT INTO "+nameOfTable+ "(PersonID,FirstName) VALUES("+idOfPerson+ ",'"+nameOFPerson+ "');";

            //SqlCommand sqlCommand = new SqlCommand("INSERT INTO Persons(PersonID,FirstName) VALUES(2,'Jay');");
            SqlCommand sqlCommand = new SqlCommand(commandString);
            sqlCommand.Connection = Con;
            var response = sqlCommand.ExecuteNonQuery();


            //// Open the connection
            //conn.Open();

            //// 1. Instantiate a new command with a query and connection
            //SqlCommand cmd = new SqlCommand("select CategoryName from Categories", conn);

            //// 2. Call Execute reader to get query results
            //rdr = cmd.ExecuteReader();

            //// print the CategoryName of each record
            //while (rdr.Read())
            //{
            //    Console.WriteLine(rdr[0]);
            //}

            var conn = Con;
            SqlCommand cmd = new SqlCommand("select FirstName from Persons", conn);

            // 2. Call Execute reader to get query results
            var rdr = cmd.ExecuteReader();
            var numberofrows = 0;
            var listofnames = new List<string>();
            // print the CategoryName of each record
            while (rdr.Read())
            {

                var temp = rdr[0].ToString();
                listofnames.Add(temp);
                numberofrows++;
            }

            var totalrows = numberofrows;
            var finalnames = listofnames;

            ShowTheContents(finalnames);



            Con.Close();

            Console.WriteLine(leavingmessage);
        }

        private static void ShowTheContents(List<string> finalnames)
        {
            var functionname = "ShowTheContents";
            var welcomemessage = "Entering " + functionname;
            var leavingmessage = "Leaving " + functionname;
            Console.WriteLine(welcomemessage);


            foreach (var x in finalnames)
            {
                Console.WriteLine(" Person Details - " + x.ToString());
            }

            var totalrows = finalnames.Count;

            Console.WriteLine("total Rows - " + totalrows);

            Console.WriteLine(leavingmessage);
        }

        //show the contents

    }
}
